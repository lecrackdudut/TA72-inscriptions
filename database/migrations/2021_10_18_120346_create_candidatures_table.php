<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidatures', function (Blueprint $table) {
            $table->id();
            $table->string('email');

            $table->longtext('motivation')->nullable();

            $table->string('c_name')->nullable();
            $table->string('c_surname')->nullable();
            $table->string('c_birthplace')->nullable();
            $table->dateTime('c_birthday')->nullable();
            $table->string('c_sex')->nullable();

            $table->string('rl1_name')->nullable();
            $table->string('rl1_surname')->nullable();
            $table->string('rl1_birthday')->nullable();
            $table->string('rl1_birthplace')->nullable();
            $table->string('rl1_sex')->nullable();
            $table->string('rl1_address')->nullable();
            $table->integer('rl1_postcode')->nullable();
            $table->string('rl1_city')->nullable();
            $table->integer('rl1_tel')->nullable();
            $table->string('rl1_mail')->nullable();

            $table->string('rl2_name')->nullable();
            $table->string('rl2_surname')->nullable();
            $table->string('rl2_birthday')->nullable();
            $table->string('rl2_birthplace')->nullable();
            $table->string('rl2_sex')->nullable();
            $table->string('rl2_address')->nullable();
            $table->integer('rl2_postcode')->nullable();
            $table->string('rl2_city')->nullable();
            $table->integer('rl2_tel')->nullable();
            $table->string('rl2_mail')->nullable();

            $table->string('c_class')->nullable();
            $table->string('c_lastclass')->nullable();

            $table->string('cl_adress')->nullable();
            $table->integer('cl_postcode')->nullable();
            $table->string('cl_city')->nullable();

            $table->boolean('i_gardmat')->nullable();
            $table->boolean('i_gardapr')->nullable();
            $table->boolean('i_rest')->nullable();
            $table->boolean('i_trans')->nullable();

            $table->integer('lastStep')->nullable();
            $table->timestamps();
            $table->foreignId('school_room_id')->nullable()->default(null)->references('id')->on('school_room');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidatures');
    }
}
