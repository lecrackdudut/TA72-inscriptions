<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class InvoiceStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('invoice_status')->insert([
            'label' => 'paid',
        ]);
        DB::table('invoice_status')->insert([
            'label' => 'pending',
        ]);
        DB::table('invoice_status')->insert([
            'label' => 'overdue-not-paid',
        ]);
        DB::table('invoice_status')->insert([
            'label' => 'late-paid',
        ]);
    }
}
