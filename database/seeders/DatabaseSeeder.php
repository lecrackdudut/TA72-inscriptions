<?php

namespace Database\Seeders;

use App\Models\InvoiceStatus;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        InvoiceStatus::create([
            'label' => 'paid',
        ]);
        InvoiceStatus::create([
            'label' => 'pending',
        ]);
        InvoiceStatus::create([
            'label' => 'overdue-not-paid',
        ]);
        InvoiceStatus::create([
            'label' => 'late-paid',
        ]);
    }
}
