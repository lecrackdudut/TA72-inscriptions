<?php

namespace App\Observers;

use App\Mail\MessageConfirmation;
use App\Models\Candidature;
use App\Models\File;
use Illuminate\Support\Facades\Mail;
use Request;

class CandidatureObserver
{
    public function updated(Candidature $candidature)
    {
        if ($candidature->isDirty('status')){
            Mail::to($candidature->email)
                ->queue(new MessageConfirmation(['message' =>
                    'Le statut de votre candidature est passé à ' . $candidature->status
                ]));
        }
    }
    public function created(Candidature $candidature)
    {
        Mail::to($candidature->email)
            ->queue(new MessageConfirmation(['message' =>
                'Votre candidature a le numéro : ' . $candidature->id.' <br>
                    Pour la retrouver vous pouvez cliquer sur ce lien :
                    <a href="'.env("APP_URL").'/'.'register/'.$candidature->id.'">Ma candidature</a>'
            ]));

        // ajouter 2 demandes de documents
        File::create([
            'fileType'=>"Dossier d'inscription",
            'validated'=>null,
            'candidature_id'=>$candidature->id
        ]);
        File::create([
            'fileType'=>"Attestation d'assurance scolaire",
            'validated'=>null,
            'candidature_id'=>$candidature->id
        ]);
    }
}
