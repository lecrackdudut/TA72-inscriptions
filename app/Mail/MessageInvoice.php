<?php

namespace App\Mail;

use Date;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MessageInvoice extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_SENDER')) // L'expéditeur
        ->subject("Nouvelle facture") // Le sujet
        ->view('emails.message-invoice') // La vue
        ->attach($this->data['path'], [
            'as' => Date::now()->toDateString() . '-facture.pdf',
            'mime' => 'application/pdf',
        ]);
    }
}
