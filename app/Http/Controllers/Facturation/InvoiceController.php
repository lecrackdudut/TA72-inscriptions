<?php

namespace App\Http\Controllers\Facturation;

use App\Models\Invoice;
use App\Models\InvoiceItem;
use App\Models\InvoiceStatus;
use App\Models\Candidature;
use App\Models\Activity;
use App\Http\Controllers\Controller;
use App\Mail\MessageInvoice;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Database\QueryException;
use Mail;
use PDF;



class InvoiceController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices = Invoice::all();
        $invoicesFormat = array();
        foreach($invoices as $invoice){
            $invoice = $this->invoiceFromDBtoApp($invoice);
            array_push($invoicesFormat,$invoice);
        }
        return $invoicesFormat;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $json = $request->all();
            $activitiess = $json["activities"];
            $activities = [];
            unset($json["activities"]);
            foreach ($activitiess as $activityID) {
                $activity = Activity::find($activityID);
                $line = [
                    "designation" => $activity->name,
                    "unit_price" => $activity->price,
                    "quantity" => 1,
                ];
                array_push($activities, $line);
            }
            $invoice = Invoice::create($json);
            $invoice->activities()->createMany($activities);
            $this->generatePDF($invoice->id);
            return new JsonResponse($this->invoiceFromDBtoApp($invoice), 200);
        } catch (QueryException $e) {
            return new JsonResponse([
                'error' => $e
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        return $invoice;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        $invoice->update($request->all());
    }

    public function calculTotal_invoice($invoice){
        // pour chaque facture, calculer le montant total

            $invoice->total = 0;
            foreach ($invoice->activities as $activity) {
                $invoice->total += $activity->unit_price * $activity->quantity;
            }
        
        return $invoice;
    }

    public function get_unpaid(Request $request)
    {
        //Cette route sert à récupérer les factures impayées, c'est-à-dire que le statut est soit "pending", soit "overdue-not-paid"
        $invoices = Invoice::whereHas('status', function($q){
            $q->where('label', 'pending')->orWhere('label', 'overdue-not-paid');
        })->get();
        // pour chaque facture, calculer le montant total
        $invoicesFormat = array();
        foreach ($invoices as $invoice) {
            $invoice = $this->invoiceFromDBtoApp($invoice);
            array_push($invoicesFormat,$invoice);
        }
        return $invoicesFormat;

    }

    public function get_stats(Request $request)
    {
        $year = $request->get("year", date("Y"));
        $mois = [
            "Janvier",
            "Février",
            "Mars",
            "Avril",
            "Mai",
            "Juin",
            "Juillet",
            "Août",
            "Septembre",
            "Octobre",
            "Novembre",
            "Décembre"
        ];
        // Chercher les invoices dont invoice_date est l'annee de $year , pour chaque mois amount représente la somme des factures et count le nombre de factures, label représente le mois, et year represente l'année, mois represente le numero de mois
        $invoices = Invoice::whereYear('invoice_date', $year)->get();
        $stats = [];
        foreach ($mois as $key => $value) {
            $stats[$key] = [
                "label" => $value,
                "year" => $year,
                "month" => $key ,
                "amount" => 0,
                "count" => 0
            ];
        }
        foreach ($invoices as $invoice) {
            $month = date("n", strtotime($invoice->invoice_date));
            $stats[$month - 1]["amount"] += $invoice->amount;
            $stats[$month - 1]["count"] += 1;
        }
        return $stats;

    }

    // cette route sort les statistiques des status des factures par mois
    public function repartitions_stats(Request $request)
    {
        $year = $request->get("year", date("Y"));
        $mois = [
            "Janvier",
            "Février",
            "Mars",
            "Avril",
            "Mai",
            "Juin",
            "Juillet",
            "Août",
            "Septembre",
            "Octobre",
            "Novembre",
            "Décembre"
        ];
        // Chercher les invoices dont invoice_date est l'annee de $year , pour chaque mois amount représente la somme des factures et count le nombre de factures, label représente le mois, et year represente l'année, mois represente le numero de mois et les status represente le nombre des factures de chaque status
        $invoices = Invoice::whereYear('invoice_date', $year)->get();
        $stats = [];
        foreach ($mois as $key => $value) {
            //filtrer les facture dont le status est pending
            $pending = $invoices->where('invoice_status_id', 1);
            //filtrer les facture dont le status est overdue-not-paid
            $overdue = $invoices->where('invoice_status_id', 2);
            //filtrer les facture dont le status est paid
            $paid = $invoices->where('invoice_status_id', 3);
            //filtrer les facture dont le status est overdue-paid
            $overdue_paid = $invoices->where('invoice_status_id', 4);


            // recuperer le nombre de factures de chaque status
            $stats[$key] = [
                "label" => $value,
                "year" => $year,
                "month" => $key + 1,
                "results" => [
                    [
                        "status" => "paid",
                        "count" => $pending->count()
                    ],
                    [
                        "status" => "pending",
                        "count" => $overdue->count()
                    ],
                    [
                        "status" => "overdue-not-paid",
                        "count" => $paid->count()
                    ],
                    [
                        "status" => "late-paid",
                        "count" => $overdue_paid->count()
                    ]
                ]
            ];
        }
        $month = $request->get("month", date("M"));
        // retourner les statistiques
        return $stats[$month - 1];
    }

    //cette route ajoute plusieurs factures 
    public function store_invoices(Request $request){
        $invoices = $request->all();
        $invoicesFormat = array();
        foreach ($invoices as $json) {
            $activitiess = $json["activities"];
            $activities = [];
            unset($json["activities"]);
            foreach ($activitiess as $activityID) {
                $activity = Activity::find($activityID);
                $line = [
                    "designation" => $activity->name,
                    "unit_price" => $activity->price,
                    "quantity" => 1,
                ];
                array_push($activities, $line);
            }
            $invoice = Invoice::create($json);
            $invoice->activities()->createMany($activities);
            $inv = $this->invoiceFromDBtoApp($invoice);
            array_push($invoicesFormat,$inv);
            $this->generatePDF($invoice->id);
        }
        return new JsonResponse($invoicesFormat, 200);
    }
    
    public function generatePDF($factureID){
        // $data=$request->all();
        // $factureID = $data['factureID'];
        $facture = Invoice::where('invoice.id',$factureID)
            ->join('candidatures','candidatures.id','invoice.student')
            ->get()[0];
        $items = InvoiceItem::where('invoice_id',$factureID)->get();

        $total = 0;
        foreach($items as $item){
            $item->prixTot = $item->quantity * $item->unit_price;
            // if ($item->designation == "Mensualite" || $item->designation == "Inscription"){
            //     $item->reduction = $facture->reduction * 100;
            //     $item->prixTot -= $prixTot * $facture->reduction;
            // }
            // else 
            $item->reduction = 0;
            $total+=$item->prixTot;
        }

        $name = $facture->rl1_name ? $facture->rl1_name : ($facture->rl2_name ? $facture->rl2_name : $facture->c_name);
        
        $data = [ 
            'idFact' => $factureID,
            'client' => [
                'nom' => $name,
                'prenom' => $facture->rl1_surname ? $facture->rl1_surname : ($facture->rl2_surname ? $facture->rl2_surname : $facture->c_surname),
                'email' => $facture->rl1_mail ? $facture->rl1_mail : ($facture->rl2_mail ? $facture->rl2_mail : 'Pas de mail renseigné')
            ],
            'items'=> $items,
            'date'=> $facture->invoice_date,
            'total'=> $total,
            'reglement'=> $facture->reglement ? "Prélèvement automatique" : "Espèce"
        ];

        $pdfName = $facture->invoice_date . "_Facture_" . $facture->id ."_". $name . ".pdf";

        $email = $facture->rl1_mail ? $facture->rl1_mail : ($facture->rl2_mail ? $facture->rl2_mail : null);

        $pdfFile = PDF::loadView('myPDF', $data)
            ->setPaper('a4', 'landscape')
            ->setWarnings(false)
            ->save(public_path("archives/" . $pdfName));

        if ($email) {
            Mail::to($email)
            ->queue(new MessageInvoice([
                'message' => 'Nouvelle facture :',
                'path' => public_path("archives/" . $pdfName) // mettre l'emplacement du PDF ici 
            ]));
        }
        
        return $pdfFile->download('facture.pdf');
    }

    public function getStudentName($student_id){
        $data = Candidature::where('id', $student_id)->select('c_name')->get()[0];
        return $data->c_name;
    }

    public function getTutorName($student_id){
        $data = Candidature::where('id', $student_id)->select('rl1_name', 'rl1_surname', 'rl2_name', 'rl2_surname','c_name', 'c_surname')->get()[0];
        return $data->rl1_name ? $data->rl1_name." ".$data->rl1_surname : ($data->rl2_name ? $data->rl2_name." ".$data->rl2_surname : $data->c_name." ".$data->c_surname);
    }

    public function getInvoiceStatus($invoice_status_id){
        $data = InvoiceStatus::where('id', $invoice_status_id)->select('label')->get()[0];
        return $data->label;
    }

    public function invoiceFromDBtoApp($invoice){
        $invoice = $this->calculTotal_invoice($invoice);
        $act = array();
        if($invoice->activities){
            foreach($invoice->activities as $activity){
                $activity = [ 
                    "id" => $activity->id,
                    "name" => $activity->designation,
                    "price" => $activity->unit_price,
                    "quantity" => $activity->quantity,
                    "description" => $activity->description
                ];
                array_push($act,$activity);
            }
        }
        $invoice = [
            "id"=> $invoice->id,
            "edition_date"=> $invoice->created_at,
            "updated_at" => $invoice->updated_at,
            "due_date"=> $invoice->due_date,
            "invoice_date" => $invoice->invoice_date,
            "price_total"=> $invoice->total,
            "student_name"=> $this->getStudentName($invoice->student),
            "billed_to"=> $this->getTutorName($invoice->student),
            "status"=> $this->getInvoiceStatus($invoice->invoice_status_id),
            "activities"=> $act
        ];
        return $invoice;
    }

}
