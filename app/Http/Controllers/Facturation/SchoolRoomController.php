<?php

namespace App\Http\Controllers\Facturation;

use App\Models\SchoolRoom;
use App\Models\Candidature;
use App\Models\Activity;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Database\QueryException;



class SchoolRoomController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return SchoolRoom::all();
    }

    public function getSchoolRoom($id){
        $schoolRoom = SchoolRoom::where('school_room.id',$id)
        ->join('candidatures','candidatures.school_room_id','school_room.id')
        ->get();
        $students = array();
        foreach($schoolRoom as $student){
            $student = [ 
                "id" => $student->id,
                "name" => $student->c_name." ".$student->c_surname,
                "legal_tutor" => $student->rl1_name ? $student->rl1_name." ".$student->rl1_surname : ($student->rl2_name ? $student->rl2_name." ".$student->rl2_surname : ''),
                "email" => $student->email
            ];
            array_push($students,$student);
        }
    
        $data = [
            "id" => $schoolRoom[0]->id,
            "label" => $schoolRoom[0]->label,
            "students" => $students,
        ];
        return $data;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $schoolRoom = SchoolRoom::create($request->all());


            return new JsonResponse($schoolRoom, 200);
        } catch (QueryException $e) {
            return new JsonResponse([
                'error' => $e
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SchoolRoom  $schoolRoom
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolRoom $schoolRoom)
    {
        return $schoolRoom;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SchoolRoom  $schoolRoom
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolRoom $schoolRoom)
    {
        $schoolRoom->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SchoolRoom  $schoolRoom
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolRoom $schoolRoom)
    {
        //
    }
}
