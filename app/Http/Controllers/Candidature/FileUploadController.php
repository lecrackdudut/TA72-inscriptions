<?php

namespace App\Http\Controllers\Candidature;

use App\Http\Controllers\Controller;
use App\Mail\NewFileMessage;
use App\Models\Candidature;
use Illuminate\Http\Request;
use App\Models\File;
use Illuminate\Support\Facades\Mail;

class FileUploadController extends Controller
{
    // function to create a new file request in 'attachedFiles' folder
    public function store(Candidature $candidature, Request $request)
    {
        $file = File::create([
            'fileType'=>$request->attachedFileType,
            'validated'=>NULL,
            'candidature_id'=>$candidature->id
        ]);
        Mail::to($candidature->email)
            ->queue(new NewFileMessage(['message' =>
                'Un nouveau document est requis pour votre candidature'
            ]));
        return $file;
    }

    // function to attache a file to a request (by a parent) or to validate a file (by an admin) in 'attachedFiles' folder
    public function update(Request $request,Candidature $candidature, File $file)
    {
        $file->update($request->all());
        if ($request->validated) {
            return response()->json(['success' => 'Vous avez validé la demande de document n°' . $file->id]);
        }
    }
    public function addFile(Request $request, Candidature $candidature, File $file){
        $f=$request->attachedFile;
        $date = time();
        $upload_path = public_path('attachedFiles/candidature ' . $candidature->id);
        $generated_new_name = $date. '.' . $f->getClientOriginalExtension();
        $f->move($upload_path, $generated_new_name);
        $file->update([
            'fileName'=>$generated_new_name,
            'validated'=>0
        ]);
        return response()->json(['success' => 'Vous avez enregistré le document ' . $file->fileType . ' en temps que : ' . $generated_new_name]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function index(Candidature $candidature)
    {
        return $candidature->files()->get();
    }


}


