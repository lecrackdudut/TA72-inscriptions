<?php

namespace App\Http\Controllers;

use App\Exports\CandidatureExport;
use App\Mail\MessageForget;
use App\Mail\MessageInvoice;
use App\Models\Candidature;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use Mail;
use Ramsey\Uuid\Uuid;

class CandidatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Candidature[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Http\Response
     */
    public function index()
    {
        return Candidature::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $candidature = new Candidature($request->all());
        $candidature->save();
        return $candidature->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Candidature  $candidature
     * @return \Illuminate\Http\Response
     */
    public function show(Candidature $candidature)
    {
        return $candidature;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Candidature  $candidature
     * @return \Illuminate\Http\Response
     */
    public function edit(Candidature $candidature)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Candidature  $candidature
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Candidature $candidature)
    {
        if ($request->get('status') == 'complete') {
            Mail::to($candidature->email)
            ->queue(new MessageInvoice([
                'message' => 'Facture pour la première échéance de l\'abonnement :',
                'path' => '/var/www/html/public/attachedFiles/candidature 1/2021_11_16.pdf' // mettre l'emplacement du PDF ici 
            ]));

            Mail::to($candidature->email)
            ->queue(new MessageInvoice([
                'message' => 'Facture pour l\'achat du matériel :',
                'path' => '/var/www/html/public/attachedFiles/candidature 1/2021_11_16.pdf' // mettre l'emplacement du PDF ici 
            ]));
        }
        
        $candidature->update($request->except(['storeAdmin']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Candidature  $candidature
     * @return \Illuminate\Http\Response
     */
    public function destroy(Candidature $candidature)
    {
        $candidature->delete();
    }

    public function export(){
        return Excel::download(new CandidatureExport, 'candidatures.xlsx');
    }
    public function resetLink(Request $request){
       $ids =  Candidature::where('email',$request->email)->pluck('id');
        Mail::to($request->email)
            ->queue(new MessageForget(['message' =>
                'Voici vos numéros de candidature :' . $ids
            ]));

    }
    // public function export(){
    //     return Excel::download(new CandidatureExport, 'candidatures.xlsx');
    // }
}
