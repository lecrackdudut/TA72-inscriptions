<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use PHPOpenSourceSaver\JWTAuth;
use Hash;
use Illuminate\Http\Request;
use Response;

class AdminController extends Controller
{

    public function login(Request $request){
        $hashedPassword =  Admin::where('username',$request->pseudo)
                ->first('password');
        if ($hashedPassword&&Hash::check($request->password, $hashedPassword->password))return response()->json([],200);
        else return response()->json([],403);
    }

    public function login_bearer_token(Request $request){
        $hashedPassword =  Admin::where('username',$request->pseudo)
                ->first('password');
                # get the user id of the user
        $user_id = Admin::where('username',$request->pseudo);
        if ($hashedPassword&&Hash::check($request->password, $hashedPassword->password)) 
        {
            # create jwt token with the id of the admin and return it
            $token = JWTAuth::fromUser($user_id);
            return response()->json(compact('token'));

        }
        else {
            # return error if the password is wrong
            return response()->json([],403);
        }
    }
    /**php artisan ide-helper:models
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        //
    }
}
