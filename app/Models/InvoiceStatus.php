<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceStatus extends Model
{
    use HasFactory;
    protected $guarded = [];

    protected $table = 'invoice_status';

    protected $hidden = array('created_at', 'updated_at');


}
