<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function status(){
        return $this->belongsTo(InvoiceStatus::class, 'invoice_status_id');
    }

    public function student(){
        return $this->belongsTo(Candidature::class, 'student');
    }


    protected $table = 'invoice';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['activities', 'status'];
 


    /**
     * Get the comments for the blog post.
     */
    public function activities()
    {
        return $this->hasMany(InvoiceItem::class);
    }

}
