<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SchoolRoom extends Model
{
    use HasFactory;
    protected $guarded = [];

    protected $table = 'school_room';

    public function students() {
        return $this->hasMany(Candidature::class);
    }


}
