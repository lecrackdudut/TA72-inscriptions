<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Candidature extends Model
{
    use HasFactory;
//    protected $with=['files'];
    protected $guarded = [];
    public function meetings(){
        return $this->hasMany(Meeting::class);
    }
    public function messages(){
        return $this->hasMany(Message::class);
    }
    public function files(){
        return $this->hasMany(File::class);
    }

    public function school_room() {
        return $this->belongsTo(SchoolRoom::class);
    }
}
