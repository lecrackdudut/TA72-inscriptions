<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model
{
    use HasFactory;
    protected $guarded = [];

    protected $table = 'invoice_item';

    protected $hidden = array('id','created_at', 'updated_at', 'invoice_id');

    public function invoice(){
        return $this->belongsTo(Invoice::class);
    }
}
