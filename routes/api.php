<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
// Specific to Subscription
Route::get('candidature/export', '\App\Http\Controllers\CandidatureController@export');
Route::resource('candidature', \App\Http\Controllers\CandidatureController::class);
Route::resource('candidature.messages', App\Http\Controllers\Candidature\MessageController::class);
Route::resource('candidature.meetings', \App\Http\Controllers\Candidature\MeetingController::class);
Route::post('/admin/login','\App\Http\Controllers\AdminController@login');
Route::resource('admin',\App\Http\Controllers\AdminController::class);

Route::resource('candidature.files', \App\Http\Controllers\Candidature\FileUploadController::class);
Route::post('resetLink','\App\Http\Controllers\CandidatureController@resetLink');
Route::post('candidature/{candidature}/files/{file}/add','\App\Http\Controllers\Candidature\FileUploadController@addFile');




// Specific to Invoicing

Route::get('repartitions', '\App\Http\Controllers\Facturation\InvoiceController@repartitions_stats')->middleware("auth");
Route::post('/invoice/generate','\App\Http\Controllers\Facturation\InvoiceController@generatePDF')->middleware("auth");
Route::post('invoices', '\App\Http\Controllers\Facturation\InvoiceController@store_invoices')->middleware("auth");

Route::get('/invoice/not-paid', '\App\Http\Controllers\Facturation\InvoiceController@get_unpaid')->middleware("auth");
Route::get('/invoice/monthly', '\App\Http\Controllers\Facturation\InvoiceController@get_stats')->middleware("auth");

Route::get('/school_room/{id}', '\App\Http\Controllers\Facturation\SchoolRoomController@getSchoolRoom')->middleware("auth");

Route::resource('activity',\App\Http\Controllers\Facturation\ActivityController::class)->only(
    'index', 'store', 'show', 'update', 'destroy'
)->middleware("auth");
Route::resource('school_room',\App\Http\Controllers\Facturation\SchoolRoomController::class)->only([
    'index', 'store', 'show', 'update', 'destroy'
])->middleware("auth");
Route::resource('invoice',\App\Http\Controllers\Facturation\InvoiceController::class)->only([
    'index', 'show', 'store'
])->middleware("auth");

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', '\App\Http\Controllers\AuthController@login');
    Route::post('logout', '\App\Http\Controllers\AuthController@logout');
    Route::post('refresh', '\App\Http\Controllers\AuthController@refresh');
    Route::post('me', '\App\Http\Controllers\AuthController@me');
    
});


