import {mapGetters} from "vuex";

const globalMixin = {
    computed: {
        state() {
            return this.$store.state;
        },
        admin() {
            return this.$store.state.storeAdmin;
        },
            ...mapGetters(['store'])
    },
    data(){
        return {
            classes:["CP","CE1","CE2","CM1","CM2"],
            STATUS: ['initial', 'waitingForMeeting','meetingDone','waitingForDocuments','complete']
        }
    },
    methods: {
        setData(type, val) {
            this.$store.commit('setData', [type, val])
        },
        setDataAdmin(type, val) {
            this.$store.commit('storeAdmin/setData', [type, val])
        },
        setStep(step){
           if (parseInt(this.state.lastStep)<parseInt(step)){
               this.setData('lastStep',step);
           }
        },
        async save(routename,redirect=true) {
            if (routename!='Dashboard') this.setStep(routename.split('Step')[1])
            else this.setData('status','waitingForDocuments')
            Object.filter = (obj, predicate) =>
                Object.keys(obj)
                    .filter(key => predicate(obj[key]))
                    .reduce((res, key) => (res[key] = obj[key], res), {});
            if (this.state.id){
                await axios.put('/candidature/' + this.state.id,
                    Object.filter(this.state, item => item !== null));
            }else{
                let response = await axios.post('/candidature',{email:this.state.email,lastStep:2});
                this.setData('id', response.data);
            }
            if (redirect)this.$router.push({name: routename});
        },
        resetStore(){
            this.$store.commit('resetStore')
        },
        async login(id){
            this.resetStore()
            await axios.get('/candidature/'+id).then((e) => {
                if (e != null) {
                    Object.entries(e.data).forEach(item=>{
                        if (item[1]!=null)this.setData(item[0],item[1]);
                    })
                    this.setData('id',id);
                    
                    if (this.store.status == 'initial') this.$router.push({name:'Step2'})
                    if (this.store.status == 'meetingDone') this.$router.push({name:'Step3'})
                    if (this.store.status == 'waitingForDocuments') this.$router.push({name:'Dashboard'})
                    if (this.store.status == 'complete') this.$router.push({name:'Dashboard'})

                    $('#modalHome').modal('hide')
                    return false

                }
            }).catch(function (error) {
                console.log(error)
                return true 
            });

        }

    }
}

export default globalMixin
