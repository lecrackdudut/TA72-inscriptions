import globalMixin from "./mixins/global";

require("./bootstrap.js")
import Vue from 'vue'
import App from './views/App'
import axios from 'axios'
import router from './plugins/router'
import store from "./plugins/store";
import storeAdmin from "./plugins/storeAdmin";
import Vuelidate from 'vuelidate'
import './../css/app.css';
window.axios = axios
axios.defaults.baseURL = process.env.MIX_APP_URL + '/api'
Vue.use(Vuelidate)
Vue.config.productionTip = false
Vue.mixin(globalMixin)
import moment from "moment";
/**
 * Fonction pour l'affichage d'une date et heure : DD/MM/YYYY HH:mm
 */
 Vue.filter('formatDateHour', function (value) {
    if (value) {
        return moment(String(value)).format('DD/MM/YYYY HH:mm')
    }
});

/**
 * Fonction pour l'affichage d'une date : DD/MM/YYYY
 */
 Vue.filter('formatDate', function (value) {
    if (value) {
        return moment(String(value)).format('DD/MM/YYYY')
    }
});

Vue.prototype.moment = moment;

new Vue({
    router,
    store,
    render: (h) => h(App),
    beforeCreate() { this.$store.commit('initialiseStore');},
}).$mount('#app')
