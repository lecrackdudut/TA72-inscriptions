import Vue from 'vue'
import VueRouter from 'vue-router'
import store from "./store";

Vue.use(VueRouter)

const routes = [
    {
        name:'Home',
        path: '/',
        component: () => import('../views/Home'),
    },
    {
        name:'Dashboard',
        path:'/dashboard/:id?',
        component: () => import('../views/Dashboard'),
    }, {
        name:'AdminDashboard',
        path:'/admin',
        component: () => import('../views/AdminDashboard'),
        beforeEnter: (to, from, next) => {
            if (store.state.storeAdmin.logged)next()
            else if (localStorage.getItem('admin')){
                next()
            }
            else next(false)
        }
    },
    {
        name: 'Registration',
        path: '/register/:id?',
        component: () => import('../views/Registration'),
        children:[
            {
                name:'Step1',
                path: '',
                component: () => import('../components/Registration/Step1'),
            },{
                name:'Step2',
                 path: '',
                component: () => import('../components/Registration/Step2'),
            },{
                name:'Step3',
                path: '3',
                component: () => import('../components/Registration/Step3'),
            },{
                name:'Step4',
                 path: '',
                component: () => import('../components/Registration/Step4'),
            },{
                name:'Step3',
                 path: '',
                component: () => import('../components/Registration/Step3'),
            },{
                name:'Step5',
                 path: '',
                component: () => import('../components/Registration/Step5'),
            },{
                name:'Step10',
                path: '',
                component: () => import('../components/Registration/Step10'),
            },
        ]
    },
]

export default new VueRouter({
    routes,
    mode: 'history',
})
